const httpStatus = require('http-status');
const { omit } = require('lodash');
const db = require('../../config/db');
/**
 * Load user and append to req.
 * @public
 */

/**
 * Get user list
 * @public
 */
exports.org = async (req, res, next) => {
  try {
    const org = await db.raw(' select * from dbo.HRCOMPANYREL ');
    res.json(org);
  } catch (error) {
    next(error);
  }
};


exports.insertTestCRUD = async (req, res, next) => {
  try {
    const data = req.body;
    const org = await db('dbo.test_crud').returning('id').insert(data);
    res.json(org);
  } catch (error) {
    next(error);
  }
};


exports.updateTestCRUD = async (req, res, next) => {
  try {
    const data = req.body;
    await db('dbo.test_crud')
      .where({ id: req.body.id })
      .update({ name: req.body.name, value: req.body.value });
    res.json('succes');
  } catch (error) {
    next(error);
  }
};

