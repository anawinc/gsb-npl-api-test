const express = require('express');
const validate = require('express-validation');

const testcontroller = require('../../controllers/test.controller');

const router = express.Router();

/**
 * Load user when API with userId route parameter is hit
 */

router
  .route('/org')
  .get(testcontroller.org);

router
  .route('/insert')
  .post(testcontroller.insertTestCRUD);

router
  .route('/update')
  .put(testcontroller.updateTestCRUD);

module.exports = router;
